﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crud.NetCore6.Shared
{
    public class Usuario
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "El largo maximo del nombre es de 50 caracteres")]
        public string? Nombre { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "El largo maximo del apellido es de 50 caracteres")]
        public string? Apellido { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string? Email{ get; set; }
        [MaxLength(15,ErrorMessage ="El largo maximo del telefono es de 15 caracteres")]
        public string? Telefono { get; set; }
 
        public DateTime? FechaAlta { get; set; }

        public DateTime? FechaBaja { get; set; }


    }
}
