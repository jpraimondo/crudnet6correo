﻿using Crud.NetCore6.Shared;
using Microsoft.EntityFrameworkCore;

namespace Crud.NetCore6.Server.Data
{
    public class UsuariosContext:DbContext
    {

        public UsuariosContext(DbContextOptions<UsuariosContext> options) : base(options)
        {

        }

        public DbSet<Usuario>? Usuarios { get; set; }
    }
}
