﻿using Crud.NetCore6.Server.Servicios.Interface;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;



namespace Crud.NetCore6.Server.Servicios
{
    public class AlmacenadorArchivo : IAlmacenadorArchivo
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly IHttpContextAccessor httpContextAccessor;

        public AlmacenadorArchivo(IWebHostEnvironment webHostEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            this.webHostEnvironment = webHostEnvironment;
            this.httpContextAccessor = httpContextAccessor;
        }




        public Task BorrarArchivo(string ruta, string contenedor)
        {
            string wwwRootPath = webHostEnvironment.WebRootPath;

            if (string.IsNullOrEmpty(wwwRootPath))
            {
                throw new Exception();
            }

            var nombreArchivo = Path.GetFileName(ruta);

            string pathFinal = Path.Combine(wwwRootPath, contenedor, nombreArchivo);

            if (File.Exists(pathFinal))
            {
                File.Delete(pathFinal);
            }

            return Task.CompletedTask;

        }

        public async Task<string> GuardarArchivo(byte[] file, string contentType, string extension, string contenedor, string nombre)
        {
            string webRootPath = webHostEnvironment.WebRootPath;

            if (string.IsNullOrEmpty(webRootPath))
            {
                throw new Exception();
            }

            string carpetaArchivo = Path.Combine(webRootPath, contenedor);
            if (!Directory.Exists(carpetaArchivo))
            {
                Directory.CreateDirectory(carpetaArchivo);
            }


            string nombreFinal = $"{nombre}{extension}";

            string rutaFinal = Path.Combine(carpetaArchivo, nombreFinal);


            await File.WriteAllBytesAsync(rutaFinal, file);

            string urlActual = $"{httpContextAccessor.HttpContext.Request.Scheme}://{httpContextAccessor.HttpContext.Request.Host}";

            string dbUrl = Path.Combine(urlActual, contenedor, nombreFinal).Replace("\\", "/");

            return dbUrl;

        }
    }

   


}     