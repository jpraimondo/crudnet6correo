﻿using Crud.NetCore6.Server.Data;
using Crud.NetCore6.Server.Servicios.Interface;
using Crud.NetCore6.Shared;
using Microsoft.EntityFrameworkCore;

namespace Crud.NetCore6.Server.Servicios
{
    public class ManejadorUsuarios : IManejadorUsuarios
    {
        readonly UsuariosContext dbContext;

        public ManejadorUsuarios(UsuariosContext context)
        {
            dbContext = context;
        }

        public void ActualizarUsuario(Usuario usuario)
        {
            try
            {
                dbContext.Entry(usuario).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {

                Console.WriteLine($"Error al listar los usuarios {ex.Message}");

                throw new Exception($"Error al listar los usuarios {ex.Message}");
            }
        }

        public void BorrarUsuario(int id)
        {
            try
            {

            var usuarioBorrar = dbContext.Usuarios?.Find(id);
            if(usuarioBorrar is not null)
            { 
            dbContext.Entry(usuarioBorrar).State = EntityState.Deleted;
            dbContext.SaveChanges();
            }

            }
            catch (Exception ex)
            {

                Console.WriteLine($"Error al borrar el usuarios {ex.Message}");

                throw new Exception($"Error al borrar el usuarios {ex.Message}");
            }

        }

        public Usuario DatosUsuario(int id)
        {
            try
            {
                Usuario? u = dbContext.Usuarios?.Find(id);
                if(u != null)
                { 
                    return u;
                }
                else
                {
                    throw new ArgumentNullException();
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine($"Error al buscar el usuarios {ex.Message}");

                throw new Exception($"Error al buscar el usuarios {ex.Message}");
            }
        }

        public async void NuevoUsuario(Usuario usuario)
        {
            try
            {
                if (usuario is not null)
                {
                  
                usuario.FechaAlta = DateTime.Now;
                //dbContext.Usuarios?.Add(usuario);
                //dbContext.SaveChanges();

                 await MailService.EnviarCorreo(usuario.Email, $"usuario.Nombre usuario.Apellido");
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine($"Error al listar los usuarios {ex.Message}");

                throw new Exception($"Error al listar los usuarios {ex.Message}");
            }
        }

        public List<Usuario> TodosLosUsuarios()
        {
            try
            {
                return dbContext.Usuarios.ToList();
            }
            catch (Exception ex)
            {

                Console.WriteLine($"Error al listar los usuarios {ex.Message}");

                throw new Exception($"Error al listar los usuarios {ex.Message}");
            }

            
        }
    }
}
