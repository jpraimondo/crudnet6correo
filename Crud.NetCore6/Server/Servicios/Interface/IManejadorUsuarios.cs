﻿using Crud.NetCore6.Shared;

namespace Crud.NetCore6.Server.Servicios.Interface
{
    public interface IManejadorUsuarios
    {

        public List<Usuario> TodosLosUsuarios();

        public void NuevoUsuario(Usuario usuario);

        public void ActualizarUsuario(Usuario usuario);

        public Usuario DatosUsuario(int id);

        public void BorrarUsuario(int id);
    }
}
