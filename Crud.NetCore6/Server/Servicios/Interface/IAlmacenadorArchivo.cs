﻿namespace Crud.NetCore6.Server.Servicios.Interface
{
    
    public interface IAlmacenadorArchivo
    {

        public Task<string> GuardarArchivo(byte[] file, string contentType, string extension, string contenedor, string nombre);

        public Task BorrarArchivo(string ruta, string contenedor);

    }



}
