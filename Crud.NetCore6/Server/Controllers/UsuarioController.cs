﻿using Crud.NetCore6.Server.Servicios.Interface;
using Crud.NetCore6.Shared;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Crud.NetCore6.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {

        private readonly IManejadorUsuarios manejadorUsuarios;

        public UsuarioController(IManejadorUsuarios iUsuarios)
        {
            manejadorUsuarios = iUsuarios;  
        }

        // GET: api/<UsuarioController>
        [HttpGet]
        public async  Task<IEnumerable<Usuario>?> Get()
        {
            try
            {
                return await Task.FromResult(manejadorUsuarios.TodosLosUsuarios());
            }
            catch (Exception ex)
            {

                return null;
            }
            
        }

        // GET api/<UsuarioController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Usuario u = manejadorUsuarios.DatosUsuario(id);
            if(u!= null)
            {
                return Ok(u);
            }
            return NotFound();
        }

        // POST api/<UsuarioController>
        [HttpPost]
        public void Post(Usuario usuario)
        {
            manejadorUsuarios.NuevoUsuario(usuario);
        }

        // PUT api/<UsuarioController>/5
        [HttpPut("{id}")]
        public void Put(Usuario usuario)
        {
            manejadorUsuarios.ActualizarUsuario(usuario);
        }

        // DELETE api/<UsuarioController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            manejadorUsuarios.BorrarUsuario(id);
        }
    }
}
